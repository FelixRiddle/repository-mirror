import os

class Var:
    update_refs: list = []
    value = None
    var_name = ""
    
    def __init__(self, var_name="", update_refs=[]):
        self.var_name = var_name
        self.update_refs = update_refs
    
    def __set__(self, parent, value):
        self.value = value
        
        rep_ser = parent.repository_services
        
        # There are not 'update references' but repository services exists
        if len(self.update_refs) < 1 and rep_ser is not None:
            # Set repository services references
            self.update_refs = rep_ser.get_repository_services_available()
        
        # If var_name exists
        if len(self.var_name) > 0:
            if len(self.update_refs) > 0:
                for update in self.update_refs:
                    # Update the variable with that name inside the reference
                    if self.var_name == "token":
                        update.token = value
                    if self.var_name == "username":
                        update.username = value
        
        if parent.debug:
            print("Var " + self.var_name + " updated to " + self.value)
    
    def __get__(self, _parent_ref, extra=None):
        return self.value

'''Get repository services class

I had a lot of problems trying to import this class from another project
when this module was added using "sys.path.append", this is the simplest
solution I found'''
def __get_repository_services():
    project_name = "sub.repository-mirror"
    
    # Test env
    if os.getcwd().endswith(project_name):
        from repository_services import RepositoryServices
        return RepositoryServices
    else: # Another project
        from . import repository_services
        return repository_services.RepositoryServices
RepositoryServices = __get_repository_services()

class RepositoryMirror:
    repository_services:RepositoryServices = None

    # When these change, update 'repository_services'
    token = Var("token")
    username = Var("username")
    
    debug = False
    
    def __init__(self, token:str="", username:str="", debug=False):
        self.token = token
        self.username = username
        self.debug = debug
        
        self.repository_services = RepositoryServices(debug=debug)
        self.repository_services.ping()
        
        if self.debug:
            print("Debug mode ON")

    '''Fetch repository by name'''
    def fetch_user_repository_json(self, repository_name: str = None):
        return self.repository_services.fetch_user_repository_json(repository_name=repository_name)
    
    '''Get a list of repositories'''
    def get_repository_list(self,
            options:dict={ "query_params": { "per_page": 100, }, "sleep": 10, },
            per_page:int=None,
            sleep:int=1,
            max_repos:int=1):
        if self.debug:
            print("RepositoryMirror.get_repository_list():")
        return self.repository_services.fetch_github_rep_data(
            options=options,
            per_page=per_page,
            sleep=sleep,
            max_repos=max_repos)
    
    def get_rate_limits(self):
        if self.debug:
            print("RepositoryMirror.get_rate_limits():")
        return self.repository_services.get_rate_limits()
    
    '''Print rate limits'''
    def print_rate_limits(self):
        if self.debug:
            print("RepositoryMirror.print_rate_limits():")
        return self.repository_services.print_rate_limits()