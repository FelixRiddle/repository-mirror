"""Handler/Wrapper for the Github rest API


"""
import dateutil.parser
import requests
import time


def get_utc_date_from_string(the_date):
    new_date = dateutil.parser.parse(the_date)
    return new_date

'''Check which date is older

Compares two dates and returns true or false depending
on which is older'''
def is_older(old_date_utc_string, new_date_utc_string):
    old_date = get_utc_date_from_string(old_date_utc_string)
    new_date = get_utc_date_from_string(new_date_utc_string)
    
    if old_date < new_date:
        return True
    else:
        return False


class GithubApi:
    api_base_url = "https://api.github.com/"
    
    # Remember to set to false in the future
    debug = False
    
    token = None
    username = None
    
    '''Format query parameters'''
    def format_query_parameters(self,
            query_params: dict = {},
            max_repos: int = 0) -> str:
        full_query_params = ""
        if query_params:
            # Accessibility type
            # Ex: all, owner, public, private
            accessibility_type = query_params.get("type", "all")
        
            # Page to request, defaults to
            page_number = str(query_params.get("page", 1))
        
            # Repositories per page
            # 100 is the max
            # The code belows defaults to 30 if the user didn't provide a value
            per_page = str(query_params.get("per_page", 30))
        
            page = query_params.get("page", 1)
            current = int(per_page) * page
        
            if current >= max_repos:
                # There's a small gap
                # page 4, per page 3: 12
                # max repos 13
                # page 5, per page 3: 15
                # max repos 13
                # base = 12
                # last: 13 - 12 = 1
                base = int(per_page) * (page - 1)
                last = max_repos - base
            
                # Now simply change per_page to last
                per_page = str(last)
                
                if last >= 1:
                    full_query_params = f"?page={page_number}&per_page={per_page}&type={accessibility_type}"
                else:
                    return None
            else:
                full_query_params = f"?page={page_number}&per_page={per_page}&type={accessibility_type}"
        return full_query_params
    
    def _show_debug(self, fn_name=""):
        if self.debug:
            print(f"github_api -> {fn_name}")

    def get_older_repositories_count(self, old_rep_list):
        self._show_debug("get_older_repositories_count():")
        return len(list(old_rep_list))

    """Get a list of older repositories

    Inserts the name of the repository in the list and
    a boolean determining if it's older or not.
    If it throws an error when trying to find the key,
    it will be set to false"""
    def get_older_list(self, prev_rep_list, new_rep_list):
        self._show_debug("get_older_list():")
        
        older_repos_list = {}
        
        for repo in prev_rep_list:
            # If the key name doesn't exist in the repo object
            if not "name" in repo:
                continue
            
            repo_name = repo["name"]
            try:
                older_repo_date = repo["pushed_at"]
                new_repo_date = repo["pushed_at"]
                
                older_repos_list[repo_name] = is_older(older_repo_date,
                    new_repo_date)
            except Exception as err:
                if self.debug:
                    print("------------------------ ERROR --------------------------")
                    print(err)
                older_repos_list[repo_name] = False
        
        return older_repos_list
    
    '''Formulate headers'''
    def headers(self, json=False):
        if self.token is None:
            raise Exception("Token doesn't exist, token: ", self.token)
        
        headers = {
            # Example token
            "Authorization": f"token {self.token}"
        }
        
        if json:
            headers["Accept"] = "application/vnd.github.v3+json"
        
        return headers
    
    '''Formulate endpoint'''
    def endpoint(self, user=False, query_params=None, max_repos:int=0):
        if not user:
            endpoint_path = f"users/{self.username}"
            endpoint = f"{self.api_base_url}{endpoint_path}"
        else:
            full_query_params = self.format_query_parameters(
                query_params,
                max_repos=max_repos)
            endpoint_path = f"user/repos"
            endpoint = f"{self.api_base_url}{endpoint_path}{full_query_params}"
        
        return endpoint

    '''Fetch repository by name'''
    def fetch_user_repository_json(self, repository_name: str = None):
        if repository_name is None:
            raise Exception("repository_name can't be none, in fetch_user_repository_json().")
        
        full_query_params = ""
    
        # Example url: https://api.github.com/felixriddle/repos
        # For organizations url: https://api.github.com/orgs/perseverancia/repos
        # Filter by type: https://api.github.com/users/octocat/repos?type=owner
    
        headers = {"Accept": "application/vnd.github.v3+json", "Authorization": f"token {self.token}"}
    
        # This only lists public repositories
        # endpoint_path = f"users/{user}/repos"
        # Get repositories of the authenticated user
        endpoint_path = f"repos/" + self.username + "/" + repository_name
        endpoint = f"{self.api_base_url}{endpoint_path}{full_query_params}"
        
        response = requests.get(endpoint, headers=headers)
        
        if self.debug:
            print("URL: ", response.url)
            print("Status code: ", response.status_code)
    
        data = response.json()
        
        return data
    
    '''Get user repositories
    
    Gets the user repositories as json
    Requires authentication'''
    def get_user_repositories(self,
            options:dict={"query_params": {}, "headers": {}},
            max_repos:int=0):
        self._show_debug("get_user_repositories")
        
        if self.token is None:
            if self.debug:
                print("Token doesn't exist!")
            
            return
        
        full_query_params = self.format_query_parameters(
            options.get("query_params"),
            max_repos=max_repos)
        
        if full_query_params is None:
            return None
        
        # Example url: https://api.github.com/felixriddle/repos
        # For organizations url: https://api.github.com/orgs/perseverancia/repos
        # Filter by type: https://api.github.com/users/octocat/repos?type=owner
        
        headers = {"Accept": "application/vnd.github.v3+json", "Authorization": f"token {self.token}"}

        # This only lists public repositories
        #enpoint_path = f"users/{user}/repos"
        # Get repositories of the authenticated user
        endpoint_path = f"user/repos"
        endpoint = f"{self.api_base_url}{endpoint_path}{full_query_params}"
        data = ""
        
        if self.debug:
            print("Endpoint: ", endpoint)
            print("Headers: ", headers)
        
        try:
            response = requests.get(endpoint, headers=headers)
            
            if self.debug:
                print("URL: ", response.url)
                print("Status code: ", response.status_code)
            
            data = response.json()
        except Exception as err:
            print("Error: ", err)
        
        return data
    
    '''Fetch user repositories from github'''
    def fetch_user_repositories_json(self,
            options:dict={ "query_params": { "per_page": 100, }, "sleep": 10},
            per_page:int=None,
            sleep:int=None,
            max_repos:int=1) -> list:
        self._show_debug("get_every_user_repository_as_json():")
        
        if per_page is not None:
            options["query_params"]["per_page"] = per_page
        if sleep is not None:
            options["sleep"] = sleep
        
        # Check if sleep exists and return its value, if not default to
        # the second parameter
        sleep_time = options.get("sleep", 10)
        page = 1
        all_repositories = []
        
        # Query params
        query_params = options.get("query_params")
        per_page = 1
        if query_params:
            # Elements per page
            per_page = query_params.get("per_page", 100)
        
        
        def fetch_repositories(new_page):
            data: list = self.get_user_repositories({
                    "query_params": {
                        "per_page": per_page,
                        "page": new_page,
                        "type": "owner"
                    }
                },
                max_repos=max_repos)
    
            if data is None:
                return True
    
            # There are no more repositories to list
            if len(data) <= 0:
                return True
            
            if 1 <= max_repos <= new_page:
                if self.debug:
                    print("Max repos reached.")
                return True
    
            for dictionary in data:
                all_repositories.append(dictionary)
            
            time.sleep(sleep_time)
        
        if max_repos >= 1:
            div = int(per_page / max_repos)
            
            for i in range(div):
                temp = fetch_repositories(page)
                if temp: break
                page += 1
        else:
            while True:
                temp = fetch_repositories(page)
                if temp: break
                page += 1
        
        # End
        return all_repositories
    
    '''Get rate limits
    
    Return rate limits'''
    def get_rate_limits(self) -> dict:
        if self.debug:
            print("get_rate_limits():")

        h = self.headers()
        
        endpoint_path = f"users/{self.username}"
        endpoint = f"{self.api_base_url}{endpoint_path}"
        
        if self.debug:
            print("Endpoint: ", endpoint)
            print("Headers: ", h)
        
        r = requests.get(endpoint, headers=h)
        data = {
            "limit": r.headers["x-ratelimit-limit"],
            "remaining": r.headers["x-ratelimit-remaining"],
            "reset": r.headers["x-ratelimit-reset"],
            "used": r.headers["x-ratelimit-used"],
        }
        return data
    
    '''Prints the rate limits to the console
    
    Makes a request to github to get the rate limits'''
    def print_rate_limits(self):
        r = self.get_rate_limits()
        
        print("Rate limit: ", r["limit"])
        print("Rate remaining: ", r["remaining"])
        print("Rate reset: ", r["reset"])
        print("Rate used: ", r["used"])
        
        return r

